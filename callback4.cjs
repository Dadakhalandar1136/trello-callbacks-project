const fs = require('fs');
const boardsInformation = require('./callback1.cjs');
const listBoardIdInformation = require('./callback2.cjs');
const cardsInformation = require('./callback3.cjs');

const allThanosInformationOne = function (bordPathFile, listPathFile, cardsPathFile, thanosId) {
    setTimeout(() => {
        boardsInformation(bordPathFile, thanosId, (error, data) => {
            if (error) {
                console.error(error);
            } else {
                console.log("All board information of thanos Id : " + thanosId);
                console.log(data);
                listBoardIdInformation(listPathFile, thanosId, (error, data) => {
                    if (error) {
                        console.error(error);
                    } else {
                        console.log("Listed the all details of thanous in Lists");
                        console.log(data);
                        const idForMind = data.find((id) => {
                            return id.name == 'Mind';
                        });
                        cardsInformation(cardsPathFile, idForMind.id, (error, data) => {
                            if (error) {
                                console.error(error);
                            } else {
                                console.log("Listed the cards data for mind Id : " + idForMind.id);
                                console.log(data);
                            }
                        });
                    }
                });
            }
        });
    }, 5 * 1000);
}

module.exports = allThanosInformationOne;