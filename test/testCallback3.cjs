const cardsInformation = require('../callback3.cjs');
const path = require('path');

const pathFile = path.resolve('data/cards.json');

function callback (error, data) {
    if (error) {
        console.error(error);
    } else {
        console.log("Listed the card details");
        console.log(data);
    }
}

cardsInformation(pathFile, 'jwkh245', callback);