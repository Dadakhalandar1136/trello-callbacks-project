const boardsInformation = require('../callback1.cjs');
const path = require('path');

const pathFile = path.resolve('data/boards.json');

const callback = function (error, data) {
    if (error) {
        console.error("Error: " + error);
    } else {
        console.log("Data fetched successfully");
        console.log(data);
    }
}

boardsInformation(pathFile, "abc122dc", callback);

