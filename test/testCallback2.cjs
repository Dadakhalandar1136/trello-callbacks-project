const listBoardIdInformation = require('../callback2.cjs');
const path = require('path');

const pathFile = path.resolve('data/lists.json');

function callback(error, data) {
    if (error) {
        console.error("Error :" + error);
    } else {
        console.log("Listed as per Id");
        console.log(data);
    }
}

listBoardIdInformation(pathFile, "mcu453ed", callback);