const fs = require('fs');

const listBoardIdInformation = function (pathFile, boardId, callback) {
    setTimeout(() => {
        fs.readFile(pathFile, 'utf-8', (error, data) => {
            if (error) {
                callback(error);
            } else {
                let allInformation = JSON.parse(data)[boardId];
                callback(null, allInformation);
            }
        });
    }, 4 * 1000);
}

module.exports = listBoardIdInformation;