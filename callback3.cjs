const fs = require('fs');

const cardsInformation = function (pathFile, listId, callback) {
    setTimeout(() => {
        fs.readFile(pathFile, 'utf-8', (error, data) => {
            if (error) {
                callback(error);
            } else {
                let cardDetails = JSON.parse(data);
                callback(null, cardDetails[listId]);
            }
        });
    }, 3 * 1000);
}

module.exports = cardsInformation;