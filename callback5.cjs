const fs = require('fs');
const boardsInformation = require('./callback1.cjs');
const listBoardIdInformation = require('./callback2.cjs');
const cardsInformation = require('./callback3.cjs');

const allThanosInformationTwo = function (bordPathFile, listPathFile, cardsPathFile, thanosId) {
    setTimeout(() => {
        boardsInformation(bordPathFile, thanosId, (error, data) => {
            if (error) {
                console.error(error);
            } else {
                console.log("All board information of thanos");
                console.log(data);
                listBoardIdInformation(listPathFile, thanosId, (error, data) => {
                    if (error) {
                        console.error(error);
                    } else {
                        console.log("Listed the all details of thanous in Lists");
                        console.log(data);
                        const idForMind = data.find((id) => {
                            return id.name == 'Mind';
                        });
                        const idForSpace = data.find((id1) => {
                            return id1.name == 'Space';
                        });
                        cardsInformation(cardsPathFile, idForMind.id, (error, data) => {
                            if (error) {
                                console.error(error);
                            } else {
                                console.log("List of card data for Mind successfully");
                                console.log(data);                    
                            }
                        });
                        cardsInformation(cardsPathFile, idForSpace.id, (error, data) => {
                            if (error) {
                                console.error(error);
                            } else {
                                console.log("List of card data for Space successfully");
                                console.log(data);
                            }
                        });
                    }
                });
            }
        });
    }, 2 * 1000);
}

module.exports = allThanosInformationTwo;