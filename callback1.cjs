const fs = require('fs');

const boardsInformation = function (pathFile, boardsData, callback) {
    setTimeout(() => {
        fs.readFile(pathFile, 'utf-8', (error, data) => {
            if (error) {
                callback(error);
            } else {
                let informationData = JSON.parse(data).filter(userId => {
                    return userId['id'] == boardsData;
                });
                callback(null, informationData);
            }
        });
    }, 5 * 1000);
}

module.exports = boardsInformation;