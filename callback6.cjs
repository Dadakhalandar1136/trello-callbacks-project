const fs = require('fs');
const boardsInformation = require('./callback1.cjs');
const listBoardIdInformation = require('./callback2.cjs');
const cardsInformation = require('./callback3.cjs');

const allThanosInformationTwo = function (bordPathFile, listPathFile, cardsPathFile, thanosId) {
    setTimeout(() => {
        boardsInformation(bordPathFile, thanosId, (error, data) => {
            if (error) {
                console.error(error);
            } else {
                console.log("All board information of thanos");
                console.log(data);
                listBoardIdInformation(listPathFile, thanosId, (error, data) => {
                    if (error) {
                        console.error(error);
                    } else {
                        console.log("Listed the all details of thanous in Lists");
                        console.log(data);
                        const listId = data.map((element) => {
                            return element.id;
                        });
                        for (let index = 0; index < listId.length; index++) {
                            cardsInformation(cardsPathFile, listId[index], (error, data) => {
                                if (error) {
                                    console.log(error);
                                } else {
                                    console.log('Card detail for the Id matched : ' + listId[index]);
                                    if (data == undefined) {
                                        console.log('No card data for this Id : ' + listId[index]);
                                    } else {
                                        console.log(data);
                                    }
                                }
                            });
                        }
                    }
                });
            }
        });
    }, 2 * 1000);
}

module.exports = allThanosInformationTwo;